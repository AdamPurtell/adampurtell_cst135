//This is my own work. Adam Purtell. CST 135

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;


public class BossGrid extends BorderPane {

	// Fields
	private Dispenser BossDisp = new Dispenser();
	private VBox centerBox = new VBox();
	private HBox rightBox = new HBox();

	// if button is pushed open boss menu in window
	// display boss configuration and options
	// allow boss to edit all values, names, prices, quantity
	public BossGrid(VendingMachineGrid MyGrid) {
		// adds MyGrid to centerBox
		centerBox.getChildren().add(MyGrid);
		// sets centerBox in center of
		this.setCenter(centerBox);
		this.setRight(rightBox);
		BossButton();

	}

	// Create Button on startup that displays in top right corner
	public void BossButton() {

		Button bossKey = new Button("Bo$$");
		rightBox.getChildren().clear();

		bossKey.setOnAction(e -> BossScreen());

		rightBox.getChildren().add(bossKey);

		rightBox.setStyle("-fx-background-color:skyblue");

	}

	// create main screen for Boss Interface
	public void BossScreen() {

		centerBox.getChildren().clear();

		Button addItems = new Button("Add Items");

		addItems.setOnAction(e -> addItems());

		Button removeItems = new Button("Remove Items");

		removeItems.setOnAction(e -> removeItems());

		Button changePrice = new Button("Change Price");

		changePrice.setOnAction(e -> changePrice());

		Button changeQuantity = new Button("Change Quantity");

		changeQuantity.setOnAction(e -> changeQuantity());

		centerBox.getChildren().addAll(addItems, removeItems, changePrice, changeQuantity);
		centerBox.setAlignment(Pos.CENTER);
		centerBox.setStyle("-fx-background-color:skyblue");

		addItems.setPrefSize(400, 75);
		addItems.setStyle("-fx-font-size: 4em");
		removeItems.setPrefSize(400, 75);
		removeItems.setStyle("-fx-font-size: 4em");
		changePrice.setPrefSize(400, 75);
		changePrice.setStyle("-fx-font-size: 4em");
		changeQuantity.setPrefSize(400, 75);
		changeQuantity.setStyle("-fx-font-size: 4em");

	}

	// method for adding Items
	public void addItems() {
		centerBox.getChildren().clear();
		Button addDrink = new Button("Add Drink");
		Button addGum = new Button("Add Gum");
		Button addChips = new Button("Add Chips");
		Button addCandy = new Button("Add Candy");

		addDrink.setOnAction(e -> newDrinkProduct());
		addGum.setOnAction(e -> newGumProduct());
		addChips.setOnAction(e -> newChipProduct());
		addCandy.setOnAction(e -> newCandyProduct());

		centerBox.getChildren().addAll(addDrink, addGum, addChips, addCandy);

		addDrink.setPrefSize(250, 75);
		addDrink.setStyle("-fx-font-size: 4em");
		addGum.setPrefSize(250, 75);
		addGum.setStyle("-fx-font-size: 4em");
		addChips.setPrefSize(250, 75);
		addChips.setStyle("-fx-font-size: 4em");
		addCandy.setPrefSize(250, 75);
		addCandy.setStyle("-fx-font-size: 4em");

	}

	// create method for removing items from machine
	public void removeItems() {

		centerBox.getChildren().clear();
		Product[] p = BossDisp.getItems();

		for (int i = 0; i < p.length; i++) {
			if (p[i] == null)
				break;
			Button btn = new Button(p[i].getProductName());
			centerBox.getChildren().add(btn);
			Product delete = p[i];
			btn.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {
					BossDisp.removeProduct(delete);
					VendingMachineGrid newVendingMachine = new VendingMachineGrid(BossDisp.displayProducts());
					centerBox.getChildren().clear();
					centerBox.getChildren().add(newVendingMachine);

				}
			});

		}

	}

	public void changePrice() {
		centerBox.getChildren().clear();
		Product[] p = BossDisp.getItems();
		TextField price = new TextField();
		Label price1 = new Label("Enter Price");
		price.setMaxWidth(50);
		centerBox.getChildren().add(price);

		for (int i = 0; i < p.length; i++) {
			if (p[i] == null)
				break;
			Button btn = new Button(p[i].getProductName());
			centerBox.getChildren().add(btn);
			Product delete = p[i];
			btn.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {

					double productPrice = Double.parseDouble(price.getText());
					BossDisp.changePrice(delete, productPrice);
					VendingMachineGrid newVendingMachine = new VendingMachineGrid(BossDisp.displayProducts());
					centerBox.getChildren().clear();
					centerBox.getChildren().add(newVendingMachine);

				}
			});

		}
	}

	public void changeQuantity() {
		centerBox.getChildren().clear();
		Product[] p = BossDisp.getItems();
		TextField quantity = new TextField();
		quantity.setMaxWidth(50);

		centerBox.getChildren().add(quantity);

		for (int i = 0; i < p.length; i++) {
			if (p[i] == null)
				break;
			Button btn = new Button(p[i].getProductName());
			centerBox.getChildren().add(btn);
			Product delete = p[i];
			btn.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {

					int productQuantity = Integer.parseInt(quantity.getText());
					BossDisp.changeQuantity(delete, productQuantity);
					VendingMachineGrid newVendingMachine = new VendingMachineGrid(BossDisp.displayProducts());
					centerBox.getChildren().clear();
					centerBox.getChildren().add(newVendingMachine);

				}
			});

		}

	}

	/////////// Drink Method /////////////////////////////

	public void newDrinkProduct() {
		centerBox.getChildren().clear();

		Label labelName = new Label("Enter Name");
		Label labelPrice = new Label("\n\n Enter Price");
		Label labelQuantity = new Label("\n\n Enter Quantity");
		TextField textName = new TextField();

		// add spinner for selection of price
		Spinner<Double> price = new Spinner<Double>();

		// Sets values of Spinner to start at .75, max out at 1.75, show 1.00
		// and decrease or increase by .25
		SpinnerValueFactory<Double> priceFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(.75, 1.75, 1.00,
				.25);

		price.setValueFactory(priceFactory);

		Spinner<Integer> quantity = new Spinner<Integer>();

		// Sets values of spinner to start at 1
		SpinnerValueFactory<Integer> quantityFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10, 1);

		quantity.setValueFactory(quantityFactory);

		Button complete = new Button("Complete");
		complete.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				Drink newDrink = new Drink();

				newDrink.setProductName(textName.getText());
				newDrink.setProductPrice(price.getValue());
				newDrink.setProductQuantity(quantity.getValue());

				if (newDrink != null) {
					if (BossDisp.getNumItems() == 8) {

						Alert MaxCap = new Alert(AlertType.ERROR, "Maximum Capacity Reached");
						MaxCap.show();
					} else {
						BossDisp.addProduct(newDrink);

						VendingMachineGrid newVendingMachine = new VendingMachineGrid(BossDisp.displayProducts());

						centerBox.getChildren().clear();
						centerBox.getChildren().add(newVendingMachine);

					}
				}

			}
		});

		centerBox.getChildren().addAll(labelName, textName, labelPrice, price, labelQuantity, quantity, complete);

	}

	/////////////////////// Gum Method /////////////////////////////

	public void newGumProduct() {
		centerBox.getChildren().clear();

		Label labelName = new Label("Enter Name");
		Label labelPrice = new Label("\n\n Enter Price");
		Label labelQuantity = new Label("\n\n Enter Quantity");
		TextField textName = new TextField();

		// add spinner for selection of price
		Spinner<Double> price = new Spinner<Double>();

		// Sets values of Spinner to start at .75, max out at 1.75, show 1.00
		// and decrease or increase by .25
		SpinnerValueFactory<Double> priceFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(.75, 1.75, 1.00,
				.25);

		price.setValueFactory(priceFactory);

		Spinner<Integer> quantity = new Spinner<Integer>();

		// Sets values of spinner to start at 1
		SpinnerValueFactory<Integer> quantityFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10, 1);

		quantity.setValueFactory(quantityFactory);

		Button complete = new Button("Complete");
		complete.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				Gum newGum = new Gum();

				newGum.setProductName(textName.getText());
				newGum.setProductPrice(price.getValue());
				newGum.setProductQuantity(quantity.getValue());

				if (newGum != null) {
					if (BossDisp.getNumItems() == 8) {

						Alert MaxCap = new Alert(AlertType.ERROR, "Maximum Capacity Reached");
						MaxCap.show();
					} else {
						BossDisp.addProduct(newGum);

						VendingMachineGrid newVendingMachine = new VendingMachineGrid(BossDisp.displayProducts());

						centerBox.getChildren().clear();
						centerBox.getChildren().add(newVendingMachine);

					}
				}

			}
		});

		centerBox.getChildren().addAll(labelName, textName, labelPrice, price, labelQuantity, quantity, complete);

		/////////////// Chips Method///////////////////

	}

	public void newChipProduct() {
		centerBox.getChildren().clear();

		Label labelName = new Label("Enter Name");
		Label labelPrice = new Label("\n\n Enter Price");
		Label labelQuantity = new Label("\n\n Enter Quantity");
		TextField textName = new TextField();

		// add spinner for selection of price
		Spinner<Double> price = new Spinner<Double>();

		// Sets values of Spinner to start at .75, max out at 1.75, show 1.00
		// and decrease or increase by .25
		SpinnerValueFactory<Double> priceFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(.75, 1.75, 1.00,
				.25);

		price.setValueFactory(priceFactory);

		Spinner<Integer> quantity = new Spinner<Integer>();

		// Sets values of spinner to start at 1
		SpinnerValueFactory<Integer> quantityFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10, 1);

		quantity.setValueFactory(quantityFactory);

		Button complete = new Button("Complete");
		complete.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				Chips newChips = new Chips();

				newChips.setProductName(textName.getText());
				newChips.setProductPrice(price.getValue());
				newChips.setProductQuantity(quantity.getValue());

				if (newChips != null) {
					if (BossDisp.getNumItems() == 8) {

						Alert MaxCap = new Alert(AlertType.ERROR, "Maximum Capacity Reached");
						MaxCap.show();
					} else {
						BossDisp.addProduct(newChips);

						VendingMachineGrid newVendingMachine = new VendingMachineGrid(BossDisp.displayProducts());

						centerBox.getChildren().clear();
						centerBox.getChildren().add(newVendingMachine);

					}
				}

			}
		});

		centerBox.getChildren().addAll(labelName, textName, labelPrice, price, labelQuantity, quantity);

	}

	///////////////////////////// Candy Method ////////////////////////
	public void newCandyProduct() {
		centerBox.getChildren().clear();

		Label labelName = new Label("Enter Name");
		Label labelPrice = new Label("\n\n Enter Price");
		Label labelQuantity = new Label("\n\n Enter Quantity");
		TextField textName = new TextField();

		// add spinner for selection of price
		Spinner<Double> price = new Spinner<Double>();

		// Sets values of Spinner to start at .75, max out at 1.75, show 1.00
		// and decrease or increase by .25
		SpinnerValueFactory<Double> priceFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(.75, 1.75, 1.00,
				.25);

		price.setValueFactory(priceFactory);

		Spinner<Integer> quantity = new Spinner<Integer>();

		// Sets values of spinner to start at 1
		SpinnerValueFactory<Integer> quantityFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10, 1);

		quantity.setValueFactory(quantityFactory);

		Button complete = new Button("Complete");
		complete.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				Candy newCandy = new Candy();

				newCandy.setProductName(textName.getText());
				newCandy.setProductPrice(price.getValue());
				newCandy.setProductQuantity(quantity.getValue());

				if (newCandy != null) {
					if (BossDisp.getNumItems() == 8) {

						Alert MaxCap = new Alert(AlertType.ERROR, "Maximum Capacity Reached");
						MaxCap.show();
					} else {
						BossDisp.addProduct(newCandy);

						VendingMachineGrid newVendingMachine = new VendingMachineGrid(BossDisp.displayProducts());

						centerBox.getChildren().clear();
						centerBox.getChildren().add(newVendingMachine);

					}
				}

			}
		});

		centerBox.getChildren().addAll(labelName, textName, labelPrice, price, labelQuantity, quantity, complete);

	}
}
