//This is my own work. Adam Purtell
//CST 135 Fritz 2/3/17

public class Chips extends Snack {

	//Fields
	private boolean spicy;
	private boolean mild;
	private boolean potato;
	private boolean pretzel;
	
	
	//Constructor
	//Initializes fields
	public Chips(String productName, double productPrice, int productQuantity, boolean containsNuts,
			boolean containsGluten, boolean spicy, boolean mild, boolean potato, boolean pretzel) {
		
		super(productName, productPrice, productQuantity, containsNuts, containsGluten);
		
		this.spicy = spicy;
		this.mild = mild;
		this.potato = potato;
		this.pretzel = pretzel;
	}
	 
	public Chips(){
		this.spicy = true;
		this.mild = false;
		this.potato = true;
		this.pretzel = false;
		
	}
	
	public Chips(Chips c){
		super(c);
		this.spicy = c.spicy;
		this.mild = c.mild;
		this.potato = c.potato;
		this.pretzel = c.pretzel;
		
	}

	
	public boolean isSpicy() {
		return spicy;
	}

	
	public void setSpicy(boolean spicy) {
		this.spicy = spicy;
	}

	
	public boolean isMild() {
		return mild;
	}

	
	public void setMild(boolean mild) {
		this.mild = mild;
	}

	
	public boolean isPotato() {
		return potato;
	}

	
	public void setPotato(boolean potato) {
		this.potato = potato;
	}

	
	public boolean isPretzel() {
		return pretzel;
	}

	
	public void setPretzel(boolean pretzel) {
		this.pretzel = pretzel;
	}

	
	@Override
	public String toString() {
		return "Chips [spicy=" + spicy + ", mild=" + mild + ", potato=" + potato + ", pretzel=" + pretzel + "]";
	}
	
	
	
	
}
