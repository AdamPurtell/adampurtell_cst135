import java.util.Arrays;

//This is my own work. Adam Purtell
//CST135 2/24/17

public class Dispenser {

	// Fields
	private int numItems;
	private Product[] items;
	private static final int Capacity = 20;

	// Constructor
	public Dispenser() {
		numItems = 0;
		items = new Product[Capacity];

		// Add products for testing
		Product p = new Drink("Cola", 1.25, 4, true, false);
		this.addProduct(p);
		p = new Chips("Flaming Hot Cheetos", 1.25, 6, true, true, false, false, true, false);
		this.addProduct(p);
		p = new Drink("AppleJuice", 1.25, 7, true, true);
		this.addProduct(p);
		p = new Gum("Big Red", 0.75, 3, true, false, true, false, false);
		this.addProduct(p);
		p = new Candy("Skittles", 1.25, 4, false, true, false, false, false);
		this.addProduct(p);
		p = new Chips("Pretzels", 1.25, 6, false, false, false, false, false, false);
		this.addProduct(p);

		Arrays.sort(items, 0, numItems);
		// remove a product test

		// p = new Chips("Lays Classic", 1.25, 6, false, false, false, false,
		// false, false);
		// Arrays.sort(items);
		// this.addProduct(p);
		// this.removeProduct(p);
	}

	public boolean addProduct(Product p) {
		if (numItems == Capacity)
			return false;
		items[numItems] = p;
		numItems++;
		return true;
	}

	public Product[] displayProducts() {
		return items;

	}

	public void changePrice(Product p, double productPrice) {
		// locate p in the array items
		int index = findProduct(p);
		// if it is not here, just return
		if (index == -1)
			return;
		// otherwise change the price of p
		items[index].setProductPrice(productPrice);
	}

	public void changeQuantity(Product p, int productQuantity) {

		int index = findProduct(p);

		if (index == -1)
			return;

		items[index].setProductQuantity(productQuantity);
	}

	private int findProduct(Product p) {

		for (int i = 0; i < numItems; i++) {
			if (p.equals(items[i]))
				return i;
		}

		return -1;
	}

	public boolean removeProduct(Product p) {
		// find p, and get index
		int i = findProduct(p);
		if (i == -1) {
			return false;
		} else {
			for (int z = i; z < numItems; z++) {
				// if last product in array show null
				if (i == 7) {
					items[i] = null;
				} else {
					items[i] = null;
					items[i] = items[++i];
					items[i] = null;

				}
			}
			numItems--;
			return true;
		}

	}

	//
	public static int getCapacity() {
		return Capacity;
	}

	
	public Product[] getItems() {
		return items;
	}

	
	public void setItems(Product[] items) {
		this.items = items;
	}

	
	public int getNumItems() {
		return numItems;
	}

	
	public void setNumItems(int numItems) {
		this.numItems = numItems;
	}

}