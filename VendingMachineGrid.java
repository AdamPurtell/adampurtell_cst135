//This is my own work. Adam Purtell
//CST135 1/27/17

import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;

public class VendingMachineGrid extends GridPane {

	
		
	
public  VendingMachineGrid(Product[] prod){
	 ItemBox ib1 = new ItemBox(prod[0]);
	 ItemBox ib2 = new ItemBox(prod[1]);
	 ItemBox ib3 = new ItemBox(prod[2]);
	 ItemBox ib4 = new ItemBox(prod[3]);
	 ItemBox ib5 = new ItemBox(prod[4]);
	 ItemBox ib6 = new ItemBox(prod[5]);
	this.add(ib1, 0, 0);
	this.add(ib2, 0, 1);
	this.add(ib3, 2, 0);
	this.add(ib4, 3, 0);
	this.add(ib5, 2, 1);
	this.add(ib6, 3, 1);
	
	
	this.setGridLinesVisible(true);
	this.setBackground();
	this.setPadding(new Insets(10, 10, 10, 10));
	

}
public void setBackground(){
	this.setStyle("-fx-background-color:skyblue");
}

}
