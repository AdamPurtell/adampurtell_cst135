//This is my own work. Adam Purtell
//CST 135 Fritz 2/3/17


public class Gum extends Snack {
	
	//Fields
	private boolean minty;
	private boolean sugarfree;
	private boolean cinnamon;
	
	//Constructor
	public Gum(String productName, double productPrice, int productQuantity, boolean containsNuts,
			boolean containsGluten, boolean minty, boolean sugarfree, boolean cinnamon) {
		
		super(productName, productPrice, productQuantity, containsNuts, containsGluten);
		
		this.minty = minty;
		this.sugarfree = sugarfree;
		this.cinnamon = cinnamon;
	}
	
	public Gum(){
		this.minty = true;
		this.sugarfree = true;
		this.cinnamon = false;
		
	}
	public Gum(Gum g){
		this.minty = g.minty;
		this.sugarfree = g.sugarfree;
		this.cinnamon = g.cinnamon;
		
		
	}
      //Getters and Setters
	
	public boolean isMinty() {
		return minty;
	}

	
	public void setMinty(boolean minty) {
		this.minty = minty;
	}

	
	public boolean isSugarfree() {
		return sugarfree;
	}

	
	public void setSugarfree(boolean sugarfree) {
		this.sugarfree = sugarfree;
	}

	
	public boolean isCinnamon() {
		return cinnamon;
	}

	
	public void setCinnamon(boolean cinnamon) {
		this.cinnamon = cinnamon;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Gum [minty=" + minty + ", sugarfree=" + sugarfree + ", cinnamon=" + cinnamon + "]";
	}

	
	
	
}
