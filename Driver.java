	//This is my own work. Adam Purtell
//CST135 1/27/17

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;



public class Driver extends Application {
	private Dispenser myDispenser = new Dispenser();
	private VendingMachineGrid myGrid = new VendingMachineGrid(myDispenser.displayProducts());
	
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();
			root.setCenter(myGrid);
			Scene scene = new Scene(root);
			
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
