//This is my own work. Adam Purtell
//CST 135 Fritz 2/3/17


public class Tea extends Drink {

	
	//Fields
	private boolean lemonFlavored;
	private boolean peachFlavored;
	private boolean sweet;
	private boolean unsweatened;
	
	
	//Constructor
	public Tea(String productName, double productPrice, int productQuantity, boolean carbonated, boolean sugarFree,
			boolean lemonFlavored, boolean peachFlavored, boolean sweet, boolean unsweatened) {
		
		super(productName, productPrice, productQuantity, carbonated, sugarFree);
		
		this.lemonFlavored = lemonFlavored;
		this.peachFlavored = peachFlavored;
		this.sweet = sweet;
		this.unsweatened = unsweatened;
	}
	
	public Tea(){
		this.lemonFlavored = false;
		this.peachFlavored = true;
		this.sweet = true;
		this.unsweatened = false;
	}
	public Tea(Tea t){
		this.lemonFlavored = t.lemonFlavored;
		this.peachFlavored = t.peachFlavored;
		this.sweet = t.sweet;
		this.unsweatened = t.unsweatened;
	}

	
	public boolean isLemonFlavored() {
		return lemonFlavored;
	}

	
	public void setLemonFlavored(boolean lemonFlavored) {
		this.lemonFlavored = lemonFlavored;
	}

	
	public boolean isPeachFlavored() {
		return peachFlavored;
	}

	
	public void setPeachFlavored(boolean peachFlavored) {
		this.peachFlavored = peachFlavored;
	}

	
	public boolean isSweet() {
		return sweet;
	}

	
	public void setSweet(boolean sweet) {
		this.sweet = sweet;
	}

	
	public boolean isUnsweatened() {
		return unsweatened;
	}


	public void setUnsweatened(boolean unsweatened) {
		this.unsweatened = unsweatened;
	}

	
	public String toString() {
		return "Tea [lemonFlavored=" + lemonFlavored + ", peachFlavored=" + peachFlavored + ", sweet=" + sweet
				+ ", unsweatened=" + unsweatened + "]";
	}
	
	
	
	
	
	
	
	
}
