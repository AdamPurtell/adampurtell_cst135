//This is my own work. Adam Purtell
//CST 135 Fritz 2/3/17

public class Drink extends Product {

	
	private boolean carbonated, sugarFree;

	public static void main(String[]  args){
	Drink test = new Drink("cola", 1.25, 4, true, false);
	System.out.println(test);
	}
	
	
	public Drink(String productName, double productPrice, int productQuantity, boolean carbonated, boolean sugarFree) {
		super(productName, productPrice, productQuantity);
		this.carbonated = carbonated;
		this.sugarFree = sugarFree;
	}
	
   public Drink(){
	   
	   super();
	   this.carbonated = true;
	   this.sugarFree = false;
			   
   }
   
   public Drink(Drink d){
	   super(d);
	   this.carbonated = d.carbonated;
	   this.sugarFree = d.sugarFree;
   }
   //Setters and Getters
/**
 * @return the carbonated
 */
public boolean isCarbonated() {
	return carbonated;
}

/**
 * @param carbonated the carbonated to set
 */
public void setCarbonated(boolean carbonated) {
	this.carbonated = carbonated;
}

/**
 * @return the sugarFree
 */
public boolean isSugarFree() {
	return sugarFree;
}

/**
 * @param sugarFree the sugarFree to set
 */
public void setSugarFree(boolean sugarFree) {
	this.sugarFree = sugarFree;
}

//toString Method  
   public String toString(){
	  String str = super.toString();
	  str += "\nCarbonated? " + this.carbonated + "Sugar free? ";
	  return str;
   }
}
