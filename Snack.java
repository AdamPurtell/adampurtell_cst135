//This is my own work. Adam Purtell
//CST 135 Fritz 2/3/17

public abstract class Snack extends Product {

	
	private boolean containsNuts;
	private boolean containsGluten;
	
	
	public Snack(String productName, double productPrice, int productQuantity, boolean containsNuts,
			boolean containsGluten) {
		super(productName, productPrice, productQuantity);
		this.containsNuts = containsNuts;
		this.containsGluten = containsGluten;
	}

	
	public Snack(){
		this.containsNuts = false;
		this.containsGluten = true;
	}
	
	public Snack(Snack s){
		super(s);
		this.containsNuts = s.containsNuts;
		this.containsGluten = s.containsGluten;
		
	}


	/**
	 * @return the containsNuts
	 */
	public boolean isContainsNuts() {
		return containsNuts;
	}


	/**
	 * @param containsNuts the containsNuts to set
	 */
	public void setContainsNuts(boolean containsNuts) {
		this.containsNuts = containsNuts;
	}


	/**
	 * @return the containsGluten
	 */
	public boolean isContainsGluten() {
		return containsGluten;
	}


	/**
	 * @param containsGluten the containsGluten to set
	 */
	public void setContainsGluten(boolean containsGluten) {
		this.containsGluten = containsGluten;
	}
	
	public String toString(){
		  String str = super.toString();
		  str += "\nContainsNuts? " + this.containsNuts + " Gluten ? " + this.containsGluten;
		  return str; 
	
	
}
}