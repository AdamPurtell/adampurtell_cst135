//This is my own work, Adam Purtell.
//CST 135 1/25/17



public abstract class Product {
	
	/*public static void main(String[] args){
		Product test1 = new Product("x", .75, 3);
		System.out.println(test1);
		Product copyProd = new Product(test1);
		System.out.println(copyProd);
		copyProd.setProductPrice(9.99);
		System.out.println(copyProd);
	}*/
	
	//Fields
	private String productName;
	private double productPrice;
	private int productQuantity;
	
	
	//Constructor
	public Product(String productName, double productPrice, int productQuantity) {
		this.productName = productName;
		this.productPrice = productPrice;
		this.productQuantity = productQuantity;
	}
	// No Argument Constructor
	
	public Product(){
		this("", 0.0, 0);
		
	//Constructor	
	}
	public Product(Product p){
		this.productName = p.productName;
		this.productPrice = p.productPrice;
		this.productQuantity = p.productQuantity;
		
	}
	// Getters and Setters
	
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public double getProductPrice() {
		return productPrice;
	}
	
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	
	public int getProductQuantity() {
		return productQuantity;
	}
	
	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}
    
	public boolean sellOne(){
		if(this.productQuantity > 0){
			this.productQuantity--;
			return true;
		}
		return false;
	}

	// toString Method
	@Override
	public String toString() {
		return "Product [productName=" + productName + ", productPrice=" + productPrice + ", productQuantity="
				+ productQuantity + "]";
	}

	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Product){
			//obj is a Product, so cast it to Product ref
			Product p = (Product)obj;
			if (p.productName.equals(this.productName) && p.productPrice == this.productPrice)
			return true;
			
		}
		//obj is not a product
		return false;
	}
	
}
