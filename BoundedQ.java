//This is my own work. Adam Purtell. CST 135//
//BOUNDED QUEUE MILESTONE 7


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

public class BoundedQ<E> implements Queue<E> {

	
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        //initialization upon creation
        BoundedQ<Integer> myQueue = new BoundedQ<>(25);
        BoundedQ<Integer> altQueue = new BoundedQ<>(5);
        
        ArrayList<Integer> lst = new ArrayList<Integer>();
        
        int [] arr = { 91, 92, 18, 96, 4, 75, 39, 40, 58, 80,
        		18, 26, 15, 19, 53, 99, 79, 87, 41, 65, 68,
        		39, 24, 72, 14};
        {  		
        for(int i : arr){
        	lst.add(i);
        }
        // test add all
        myQueue.addAll(lst);
        System.out.println(myQueue.peek() + " = front of myQueue");
        System.out.println("myQueue.size() = " + myQueue.size());
        
        int val;
        while(!myQueue.isEmpty()){
        	// while there is 
        	if(myQueue.peek()%2 == 0){
        		//if the front of the queue is even
        	if(!altQueue.add(val = myQueue.remove()))
        		System.out.println(" altQueue is full. cannot add" + val);
        	
        	}else{
        	myQueue.remove();
        	
        	System.out.println("front of queue is not even");
        	}
        	
        }
        System.out.println("myQueue.size() == " + myQueue.size());
        System.out.println("altQueue.size() == " + altQueue.size());
        
        for(int i : lst)
        	myQueue.add(i);
        
        ArrayList<Integer> temp = new ArrayList<Integer>();
        
        temp.add(75); temp.add(80);
        temp.add(39); temp.add(40);
        temp.add(58);
        
        if(myQueue.containsAll(temp))
        	System.out.println("myQueue contains all elements in temp");
        
        else
        	System.out.println(" It Dosent");
        }
        }
        

    //INSTANCE VARIABLE
    private int front;
    private int back;
    private int numElements;
    private final int CAP;
    private E[] arr;
    
    
    public BoundedQ(int capacity){
        front = back = numElements = 0;
        CAP = capacity;
        //CAST TO NEW OBJECT
        arr = (E[]) new Object[CAP];
    }


    
    //front = (front+1)%CAP//
    //back = (back+1)%CAP//
    
    @Override
    public int size() {
        // TODO Auto-generated method stub
        return this.numElements;
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return this.numElements == 0;
    }
@Override
    public boolean contains(Object o) {
        // TODO Auto-generated method stub
		for(int i = front; i < front + numElements; i ++)
		
			if(o.equals(arr[i%CAP])){
				return true;
			}
		return false;
        
    }

    @Override
    public Object[] toArray(){
        // TODO Auto-generated method stub
       E[] temp =(E[]) new Object[numElements];
       int j = front;
       for(int i = 0; i < numElements; i++){
    	   temp[i] = arr[j++];
    	   j%=CAP;
    	   
       }
       return temp;
    }
  
    

    @Override
    public boolean remove(Object o) {
        // TODO Auto-generated method stub
        for(int i = 0; i < numElements; i++){
            if((Integer) o == arr[i]){
            for(int j = i; j < numElements; j++){
            if(j == numElements -1){
            back = j;
            //System.out.println("--Back is " + back);
            numElements--;
            System.out.println("\tSelected Item removed: " + o.toString());
               return true;
                    }
                    if(j + 1 < numElements){
                        arr[j] = arr[j + 1];
                    }

                }

            }
        }
        return false;
    }
@Override
    public boolean containsAll(Collection<?> c) {
        // TODO Auto-generated method stub
	for(Object e : c){
		if(this.contains(e))
			return true;
	}
			return false;
	}

 	//Implement this method
    @Override
    public boolean addAll(Collection<? extends E> c) {
        // TODO Auto-generated method stub
        for(E e : c){
        	if(add(e));
        	else return false;
        }
        return true;
        
    }
        
    

    @Override
    public boolean removeAll(Collection<?> c) {
        // TODO Auto-generated method stub
        if(c.isEmpty())
            return false;
        for(int i = c.size(); i > 0; i--){
            System.out.println(c.size());
            System.out.println(i);
            c.remove(((Queue<E>) c).peek());

        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void clear() {
        // TODO Auto-generated method stub
        this.numElements = 0;
        front = back = 0;
    }

    @Override
    public boolean add(E e) {
        // TODO Auto-generated method stub
        if(numElements == CAP)
            return false;

        // if not then do
        
        this.arr[back] = e;
        back = (back + 1) % CAP;
        numElements++;
        return true;
    }
    //IMPLEMENT
    @Override
    public boolean offer(E e) {
        // TODO Auto-generated method stub
        return false;
    }
    //IMPLEMENT
    @Override
    public E remove() {
    	if(isEmpty())
    		return null;
    	E temp = arr[front];
    	front = (front +1) % CAP;
    	numElements--;
    	return temp;
        /*// TODO Auto-generated method stub
        if(numElements == 0) return null;
        E temp = arr[front];
        front %= CAP;
        return temp;
*/
    }

    //IMPLEMENT
    @Override
    public E peek() {
        // TODO Auto-generated method stub
        if(numElements != 0) return arr[front];
        return null;
         
    }

////////////////////////UNIMPLEMENTED METHODS BELOW/////////////////////////////////////////


	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public E element() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public E poll() {
		// TODO Auto-generated method stub
		return null;
	}
}
