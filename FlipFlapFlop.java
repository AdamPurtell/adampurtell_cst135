
//This is my own Work. Adam Purtell
//CST 135 4/4/17

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FlipFlapFlop {



   public static void main(String [] args) throws FileNotFoundException {
	   //Create Scanner to read from file FFF.in
     Scanner scan = new Scanner(new File ("FlipFlapFlop.in"));
     
       int test = scan.nextInt();
       String[] words = new String[test];
       int i = 0;
       while(scan.hasNext() && i < test){
       String str = scan.next().toUpperCase();
       words[i] = str;
       i++;
       }
       
       System.out.println(test);
       for(int j = 0; j < words.length; j++){
    	   
       //reassign value to limit characters
       System.out.format("%-25S", words[j]);
       //Output string in all UPPERCASE
       
       System.out.println((isFlop( words[j]) + "").toUpperCase());
       
       
                }

        }
// Create a method to identify a FLAP 
        public static boolean isFlap(String str){
                if(str.length()<3)
                        return false;
                if(     str.charAt(0)!= 'D' && str.charAt(0)!='E' )
                        return false;
                int i = 1;
                for(;i<str.length() && str.charAt(i)=='F'; i++);
                if(i==str.length())
                        return false;
                //i is on index of first non-F
                //is the next letter a G
                if(str.charAt(i)=='G' && i==str.length()-1)
                        return true;
                else
                        return isFlap(str.substring(i));

        }
        
// Create method to identify a FLIP
        
        public static boolean isFlip(String str){
                if(str.charAt(0) != 'A')
                        return false;
                if(str.charAt(1) != 'H' && str.length()==2)
                        return true;
                if(str.charAt(str.length() - 1) != 'C')
                        return false;
                if(str.charAt(1) == 'B'){
                        if(str.length() < 5)
                                return false;
                        String subString = str.substring(2, str.length() - 1);
                        return isFlip(subString);
                }
                else{
                        String subString = str.substring(1, str.length() - 1);
                        return isFlap(subString);
                }
        }
// Create a method to identify a FLOP
        public static boolean isFlop(String str){
                if(str.length() < 5)
                        return false;
                if(str.charAt(0) != 'A')
                        return false;
                if(str.charAt(1) != 'H'){
                        int i = str.length() - 1;
                        for(; i >= 0; i--)
                                if(str.charAt(i) == 'C')
                                        break;
                        String flip = str.substring(0, i + 1);
                        if(isFlip(flip)){
                                String flap = str.substring(i + 1);
                                if(isFlap(flap))
                                        return true;
                        }
                }else{
                                String flap = str.substring(2);
                                return isFlap(flap);
                        }

                return false;

        }


}


