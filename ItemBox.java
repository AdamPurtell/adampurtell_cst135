//This is my own work. Adam Purtell
//CST135 1/27/17

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class ItemBox extends VBox {
	
	private ImageView imgV;
	private Text name;
	private Button price;
	private Product prod;
	
	
	public ItemBox(Product prod){
	this.setPrefSize(200, 200);	
	 this.prod = prod;
	 this.setAlignment(Pos.CENTER);
	imgV = new ImageView(SeeImage());
	imgV.setFitHeight(100);
	imgV.setPreserveRatio(true);
	name = new Text(prod.getProductName());
	price = new Button(prod.getProductPrice() + "" + "");
	
	
	this.getChildren().addAll(imgV,name,price);
	
	}
	public Image SeeImage(){
		
		if(prod instanceof Chips)
			return new Image("lays-classic.png");
		else if(prod instanceof Drink)
			return new Image("DrPepper.png");
		else if(prod instanceof Candy)
			return new Image("Snickers.png");
		else if(prod instanceof Gum)
			return new Image("BigRed.png");
		else return new Image("");
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
}
