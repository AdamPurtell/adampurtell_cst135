//This is my own work. Adam Purtell
//CST 135 Fritz 2/3/17


public class Candy extends Snack {
	
	//Fields
	private boolean sweet;
	private boolean sour;
	private boolean containsChocolate;
	private boolean containsNuts;
	
	 
	public Candy(String productName, double productPrice, int productQuantity, boolean containsNuts,
			boolean containsGluten, boolean sweet, boolean sour, boolean containsChocolate) {
		super(productName, productPrice, productQuantity, containsNuts, containsGluten);
		this.sweet = sweet;
		this.sour = sour;
		this.containsChocolate = containsChocolate;
		this.containsNuts = containsNuts;
	}

	public Candy(){
		this.sweet = true;
		this.sour = false;
		this.containsChocolate = true;
		this.containsNuts = true;
		
	}
	public Candy(Candy ca){
		super(ca);
		this.sweet = ca.sweet;
		this.sour = ca.sour;
		this.containsChocolate = ca.containsChocolate;
		this.containsNuts = ca.containsNuts;
	}

	
	public boolean isSweet() {
		return sweet;
	}


	public void setSweet(boolean sweet) {
		this.sweet = sweet;
	}

	
	public boolean isSour() {
		return sour;
	}

	
	public void setSour(boolean sour) {
		this.sour = sour;
	}

	
	public boolean isContainsChocolate() {
		return containsChocolate;
	}

	
	public void setContainsChocolate(boolean containsChocolate) {
		this.containsChocolate = containsChocolate;
	}

	
	public boolean isContainsNuts() {
		return containsNuts;
	}

	
	public void setContainsNuts(boolean containsNuts) {
		this.containsNuts = containsNuts;
	}


	
	public String toString() {
		return "Candy [sweet=" + sweet + ", sour=" + sour + ", containsChocolate=" + containsChocolate
				+ ", containsNuts=" + containsNuts + "]";
	}
	
	
	

}
